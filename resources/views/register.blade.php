<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buat Account</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <p>
            <label for="fname">First name:</label><br>
            <input type="text" name="fname" value="">
        </p>
        <p>
            <label for="lname">Last name:</label><br>
            <input type="text" name="lname" value="">
        </p>
        <p> 
            <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
        </p>
        <p>
            <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select>     
        </p>
        <p>
            <p>Languange Spoken:</p>
            <input type="checkbox" id="language1" name="language1" value="indonesian" >
            <label for="language1">Bahasa Indonesia</label><br>
            <input type="checkbox" name="languange2" id="languange2" value="english">
            <label for="language2">English</label><br>
            <input type="checkbox" name="language3" id="languange3" value="other">
            <label for="languange3">Others</label>
        </p>
        <p>
            <label for="bio">Bio:</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </p>
        <p>
            <input type="submit" value="Sign Up">
        </p> 
    </form>
</body>
</body>
</html>